import { BrowserRouter as Router, Switch, Route, } from "react-router-dom";
import "./App.css";
import Products from "./components/products";

const App = () => {
  return (

      <Router>
        <Switch>
          <Route path={'/'} component={Products}/>
        </Switch>
      </Router>
  );
}

export default App;
