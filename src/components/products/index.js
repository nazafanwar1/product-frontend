import React, { useState, useEffect } from "react";
import styled from "styled-components";
import PageHeader from "./pageHeader";
import Table from "./Table";
import axios from "axios";
const Styles = styled.div`
`;

const columns=[
  {
    title: "",
    field: "imageUrl",
    render: rowData =><img src={rowData.imageUrl} style={{width: 100}} alt="description"/> 
  },
  {
    title: "",
    field: "photoUrl",
    render: rowData =><img src={rowData.photoUrl} style={{width: 100}} alt="description"/>
  },
  {
    title: "",
    field: "portraitUrl",
    render: rowData =><img src={rowData.portraitUrl} style={{width: 100}} alt="description"/>
  },
  {
    title: "",
    field: "description"
  },
  {
    title: "",
    field: "price"
  }
];

const Product = () => {
  const [products, setProducts] = useState([]);

  const fetchData = async () => {
    const result = await axios.get("https://u1i8q5y65f.execute-api.us-east-2.amazonaws.com/development/products");
    setProducts(result.data.map(obj => ({
      ...obj,
      imageUrl: "https://source.unsplash.com/featured/150x150/?bedsheet",
      photoUrl: "https://source.unsplash.com/featured/150x150/?blanket",
      portraitUrl: "https://source.unsplash.com/featured/150x150/?pillow"
    })))
  }

  useEffect(() => {
    fetchData();
  }, []);

    return (
      <Styles>
        <PageHeader/>
        <Table col={columns} data={products} />
      </Styles>
    );

};

export default Product;
